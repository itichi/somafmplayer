let content = document.getElementById("content");
let base_somafm_url = 'https://somafm.com/player/#/now-playing/';

function load_radio() {
    let select_name = document.getElementById('radio_name');
    let name = select_name.options[select_name.selectedIndex].text;
    select_name.options[select_name.selectedIndex].selected = true;
    let url = base_somafm_url + name;
    console.log('Radio name: ' + name);
    console.log('Radio url: ' + url);
    content.innerHTML = '<object style="float:left; width:100%;height:100%;" type="text/html" data="' + url + '" ></object>';
}

const remote = require('electron').remote;

function quitMe() {
  var window = remote.getCurrentWindow();
  window.close();
}

function minimize(){
  var window = remote.getCurrentWindow();
  window.minimize();  
}

document.addEventListener("keydown", event => {
  switch (event.key) {
      case "Escape":
          var window = remote.getCurrentWindow();
          window.close();
          break;
       }
});
